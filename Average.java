package ru.priborka;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Класс для вычисления среднего балла по группам
 *
 * @author A.A.Simonova
 */
public class Average {
    public static final String GR15OIT18 = "15ОИТ18";
    public static final String GR15OIT20 = "15ОИТ20";
    public static final String FORMAT = "Средний балл в группе %s равен %5.2f%n";
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int numberOf15oit18;
        int numberOf15oit20;
        numberOf15oit18 = getNumberOfGroup(GR15OIT18);
        numberOf15oit20 = getNumberOfGroup(GR15OIT20);
        int[] gr15oit18 = new int[numberOf15oit18];
        int[] gr15oit20 = new int[numberOf15oit20];
        init(gr15oit18);
        System.out.println(Arrays.toString(gr15oit18));
        init(gr15oit20);
        System.out.println(Arrays.toString(gr15oit20));

        System.out.printf(FORMAT, GR15OIT18, average(gr15oit18));
        System.out.printf(FORMAT, GR15OIT18, average(gr15oit20));
    }

    /**
     * Возвращает средний балл по группе
     * @param group массив оценок
     * @return средний балл
     */
    private static double average(int[] group) {
        double sum = 0;
        for (int rating : group) {
            sum += rating;
        }
        return sum / group.length;
    }

    /**
     *
     * @param group
     */
    private static void init(int[] group) {
        for (int i = 0; i < group.length; i++) {
            group[i] = 2 + (int) (Math.random() * 4);
        }
    }

    private static int getNumberOfGroup(String group) {
        System.out.println("Введите количество студентов в группе " + group + ":");
        return scanner.nextInt();
    }
}


